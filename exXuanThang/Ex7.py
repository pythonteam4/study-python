# nhập 1 chuỗi có (tối đa) 10 phần tử. Hiển thị chuỗi vừa nhập ra màn hình.
# In ra màn hình các giá trị sau: chiều dài của chuỗi, số lượng kí tự là dấu '  ' (khoảng trắng) trong chuỗi.
# Chuyển chuỗi sang chữ HOA và xuất ra màn hình
print("Nhập một chuỗi (tối đa mười phần tử):")
chain = str(input())
x = len(chain)
y = chain.count(' ')
if x > 10:
    print("Chuỗi đã nhập quá số lượng cho phép")
else:
    print(str(chain))
    print("Chiều dài chuỗi là: "+str(x)+", số lượng khoảng trắng là: "+str(y))
    print(chain.upper())