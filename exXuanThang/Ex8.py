# nhập họ và tên.
# In ra màn hình họ tên sau khi chuẩn hóa (Tất cả chữ cái đầu đều viết hoa, toàn bộ tên viết HOA).
fullName = str(input("Nhập họ và tên:"))
surname, name = fullName.rsplit(" ",1)
print(str(surname.capitalize())+" "+str(name.upper()))
