# nhập lần lượt các giá trị (số nguyên) cho 1 List có 5 phần tử.
# Hiển thị danh sách vừa nhập ra màn hình.
# In ra màn hình các giá trị: lớn nhất, nhỏ nhất, giá trị trung bình các phần tử của danh sách.
print("Nhập lần lượt các giá trị số nguyên (5 phần tử): ")
list = []
for i in range(0, 5):
    x = int(input())
    list.append(x)

print(list)
print("Max: "+str(max(list)))
print("Min: "+str(min(list)))
print("Average: "+str(sum(list)/len(list)))