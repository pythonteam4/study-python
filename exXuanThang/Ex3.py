#  nhập vào giá trị điện dung C, điện cảm L và tần số f
#  Tính và in ra tổng trở Z của mạch
import math
print("Nhập giá trị điện dung C :")
c = float(input())
print("Nhập giá trị điện cảm L :")
l = float(input())
print("Nhập giá trị tần số f :")
f = float(input())
z = float(abs(2*math.pi*l-1/(2*math.pi*f*c)))
print("Với C = "+str(c)+",L = "+str(l)+",f = "+str(f)+".Tổng trợ của mạch Z = "+str(z))