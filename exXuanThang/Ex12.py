# Tạo sẵn 1 danh sách 2 chiều (danh sách lồng ghép) có 5 phần tử (mỗi phần tử có 3 giá trị: tương ứng – Họ tên, MSSV, Điểm).
# Hiển thị danh sách vừa nhập ra màn hình.
# Cho phép người dùng nhập vào MSSV (hoặc Họ tên). In điểm số tương ứng ra màn hình.
import numpy

a = numpy.array([['Nguyen dang A', '20130370', 10],
                 ['Nguyen dang B', '20430730', 9],
                 ['Nguyen dang C', '20170730', 8],
                 ['Nguyen dang D', '26130730', 7],
                 ['Nguyen dang C', '20135730', 6]])
print(str(a))
print("Nhập mã số sinh viên (hoặc họ và tên)")
x = str(input())
if x == a[0, 0] or x == a[0, 1]:
    print(a[0, 2])
elif x == a[1, 0] or x == a[1, 1]:
    print(a[1, 2])
elif x == a[2, 0] or x == a[2, 1]:
    print(a[2, 2])
elif x == a[3, 0] or x == a[3, 1]:
    print(a[3, 2])
elif x == a[4, 0] or x == a[4, 1]:
    print(a[4, 2])
else:
    print("Không có mã số sinh viên (hoặc họ và tên sinh viên) này trong danh sách.")
