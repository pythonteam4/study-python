# GẮN NHIỀU BIẾN CÙNG MỘT GIÁ TRỊ
# multiple assignment = cho phép chúng tôi gán nhiều biến cùng một lúc trong một dòng mã

name = "Bro"
age = 21
attractive = True

name, age, attractive = "Bro", 21, True

print(name,end =" ")
print(age)
print(attractive)

# Spongebob = 30
# Patrick = 30
# Sandy = 30
# Squidward = 30

# Spongebob = Patrick = Sandy = Squidward = 30

# print(Spongebob)
# print(Patrick)
# print(Sandy)
# print(Squidward)