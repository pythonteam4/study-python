# Nhập vào ngày tháng năm sinh theo định dạng ‘d:m:yyyy’
# Tách và in riêng giá trị: ngày, tháng, năm
birthday = input("Nhập vào ngày tháng năm sinh theo định dạng ‘d:m:yyyy’ :")
day, month, year = birthday.split(":")
day = birthday[:birthday.find(":")]
month = birthday[birthday.find(":")+1:birthday.rfind(":")]
year = birthday[birthday.rfind(":")+1:]
print("Ngày: ",day)
print("Tháng: ", month)
print("Năm: ", year)