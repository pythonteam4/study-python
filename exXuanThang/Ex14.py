#hàm Fibonacci(n) với n là số tự nhiên.
#Hàm trả về 1 danh sách với các phần tử tương ứng là giá trị của dãy số Fibonacci có n giá trị
def fibonacci(n):
    lst = [0, 1]
    for i in range(2, n):
        lst.append(lst[i - 1] + lst[i - 2])
    print(str(lst))

print(fibonacci(10))