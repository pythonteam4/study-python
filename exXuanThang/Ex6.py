#nhập vào giá trị chiều cao(m) và cân nặng (kg) theo định dạng số thực
#Tính chỉ số BMI dựa theo thông tin vừa nhập và in kết quả BMI ra màn hình kèm theo thông tin đánh giá
#BMI = cân nặng/(chiều cao*chiều cao)
print("Nhập chiều cao (m): ")
height = float(input())
print("Nhập cân nặng (kg):")
weight = float(input())
bmi = round(weight / (height * height))
if bmi < 18.5:
    print("BMI = "+str(bmi)+" => "+"Gầy")
elif 18.5 <= bmi <= 25:
    print("BMI = " + str(bmi) + " => " + "Bình thường")
elif 25 < bmi <= 30:
    print("BMI = "+str(bmi)+" => "+"Thừa cân")
else:
    print("BMI = "+str(bmi)+" => "+"Béo phì")
