# nhập 1 chuỗi (tối đa 50 kí tự).
# Xóa hết tất cả kí tự khoảng trắng trong chuỗi '  ' và xuất chuỗi mới ra màn hình
print("Nhập chuỗi (tối đa 50 kí tự):")
chain = str(input())
print(str(chain.replace(" ", ""))) if len(chain) <= 50 else print("Chuỗi đã nhập nhiều hơn số ký tự cho phép")