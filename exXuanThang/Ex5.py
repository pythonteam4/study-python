# nhập vào các số km đi được.
# Xuất ra màn hình tiền cước dựa trên bảng giá đã cho.
print("Số km đã đi được :")
x = int(input())
if x <= 2:
    print("Tiền cước cho " + str(x) + " km là: " + str(x * 10000) + " đ")
elif 2 < x <= 10:
    y = x - 2
    print("Tiền cước cho " + str(x) + " km là: " + str(y * 7000 + 20000) + " đ")
elif 10 < x <= 20:
    z = x - 10
    print("Tiền cước cho " + str(x) + " km là: " + str(z * 5000 + 76000) + " đ")
else:
    t = x - 20
    print("Tiền cước cho " + str(x) + " km là: " + str(t * 3000 + 126000) + " đ")
