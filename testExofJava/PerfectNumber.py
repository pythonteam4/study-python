# kiểm tra có phải số hoàn hảo không
def check_Perfect_Number(x):
    temp = 0
    for i in range(1, x - 1):
        if x % i == 0:
            temp += i
    if temp == x:
        return True
    return False


# in danh sách số hoàn hảo
def print_Perfect_Number(x):
    for i in range(1, x - 1):
        if check_Perfect_Number(i):
            print(i,end =" ")


# print(check_Perfect_Number(28))
print(print_Perfect_Number(10))
