#PHƯƠNG THỨC CHUỖI

name = "Bro"

print(len(name)) #Chiều dài chuỗi
print(name.find("o")) #Tìm vị trí của kí tự
print(name.capitalize())#In ra chuỗi viết hoa chữ cái đầu
print(name.upper())#In hoa full chuỗi
print(name.lower())#In thường full chuỗi
print(name.isdigit())#Full chữ số => True
print(name.isalpha())#Full chữ cái => True
print(name.count("o"))#Kiểm tra chữ cái có xuất hiện trong chuỗi ko.Có =>1, không =>0
print(name.replace("o","a"))#Thay chữ o => a
print(name*3)#in ra ba lần chuỗi
