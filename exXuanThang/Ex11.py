# nhập một giá trị số nguyên, nếu nhập sai định dạng yêu cầu nhập lại giá trị.
# Tạo một List (danh sách) với các phần tử là số nguyên (giá trị tuân theo quy luật của dãy số Fibonacci) và số lượng phần tử bằng với giá trị vừa nhập.
# Hiển thị danh sách vừa nhập ra màn hình.
# In ra màn hình tất cả các giá trị là số nguyên tố có trong danh sách vừa nhập.
while True:
    try:
        x = int(input("Nhập một giá trị số nguyên:"))
    except ValueError:
        print("Nhập sai, vui lòng nhập lại!!!")
        continue
    else:
        break
lst = [0 , 1]
for i in range(2, x):
    lst.append(lst[i - 1] + lst[i - 2])
print(str(lst))