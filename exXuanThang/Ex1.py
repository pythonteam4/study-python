#Cho phép nhập vào một số nguyên có 3 chữ số
#In ra màn hình chữ số hằng trăm, hàng chục và hàng đơn vị tương ứng của số vừa nhập.

print("Nhập số nguyên có ba chữ số")
x = int(input());
y = x % 100
donvi = int(y % 10)
chuc = int(y / 10)
tram = int(x / 100)

if x > 99 and x < 1000:
    print("Chữ số hàng trăm là: "+str(tram))
    print("Chữ số hàng chục là: " + str(chuc))
    print("Chữ số hàng đơn vị là: " + str(donvi))
else:
    print("Số đã nhập không phải là số có ba chữ số")